package com.julliana.DAO;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.julliana.Classes.Games.Ranking;
import com.julliana.DAO.InterfacesDAO.RankingIDAO;

public class RankingDAO implements RankingIDAO{
    private EntityManagerFactory emf;

    public RankingDAO() {
        emf = Persistence.createEntityManagerFactory("my-persistence-unit");
    }

    @Override
    public void saveRanking(Ranking ranking) {
        EntityManager em = emf.createEntityManager(); // pra cada operação vai criar um entity manager - ele recebe um
                                                      // objeto pessoa
        em.getTransaction().begin(); // abre a conexão no db
        em.persist(ranking); // persiste no db
        em.getTransaction().commit(); // comita no db - commit é só depois que persiste e deu certo, se nao ele dara
                                      // um rollback no db
        em.close();
    }

    @Override
    public Ranking postRankingByID(Long id) {
        EntityManager em = emf.createEntityManager();
        Ranking ranking = em.find(Ranking.class, id);
        em.close();
        return ranking;
    }

    @Override
    public List<Ranking> postAllRanking() {
        EntityManager em = emf.createEntityManager();
        Query query = em.createQuery("SELECT a FROM Ranking a");
        List<Ranking> rankings = query.getResultList();
        em.close();
        return rankings;
    }

    @Override
    public void putRanking(Ranking ranking) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.merge(ranking);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteRanking(Ranking ranking) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        ranking = em.merge(ranking);
        em.remove(ranking);
        em.getTransaction().commit();
        em.close();
    }
}
