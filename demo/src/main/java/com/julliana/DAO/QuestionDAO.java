package com.julliana.DAO;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import com.julliana.Classes.Games.Question;
import com.julliana.DAO.InterfacesDAO.QuestionIDAO;

public class QuestionDAO implements QuestionIDAO {
    private EntityManagerFactory emf;

    public QuestionDAO() {
        emf = Persistence.createEntityManagerFactory("my-persistence-unit");
    }

    @Override
    public void getQuestion(Question question) {
        EntityManager em = emf.createEntityManager(); // pra cada operação vai criar um entity manager - ele recebe um
                                                      // objeto pessoa
        em.getTransaction().begin(); // abre a conexão no db
        em.persist(question); // persiste no db
        em.getTransaction().commit(); // comita no db - commit é só depois que persiste e deu certo, se nao ele dara
                                      // um rollback no db
        em.close();
    }

    @Override
    public Question postQuestionByID(Long id) {
        EntityManager em = emf.createEntityManager();
        Question question = em.find(Question.class, id);
        em.close();
        return question;
    }

    @Override
    public List<Question> postAllQuestion() {
        EntityManager em = emf.createEntityManager();
        Query query = em.createQuery("SELECT a FROM Question a");
        List<Question> questions = query.getResultList();
        em.close();
        return questions;
    }

    @Override
    public void putQuestion(Question question) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.merge(question);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteQuestion(Question question) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        question = em.merge(question);
        em.remove(question);
        em.getTransaction().commit();
        em.close();
    }
}
