package com.julliana.DAO;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import com.julliana.Classes.Games.Partida;
import com.julliana.DAO.InterfacesDAO.PartidaIDAO;

public class PartidaDAO implements PartidaIDAO{
        private EntityManagerFactory emf;

    public PartidaDAO() {
        emf = Persistence.createEntityManagerFactory("my-persistence-unit");
    }

    @Override
    public void getPartida(Partida partida) {
        EntityManager em = emf.createEntityManager(); // pra cada operação vai criar um entity manager - ele recebe um
                                                      // objeto pessoa
        em.getTransaction().begin(); // abre a conexão no db
        em.persist(partida); // persiste no db
        em.getTransaction().commit(); // comita no db - commit é só depois que persiste e deu certo, se nao ele dara
                                      // um rollback no db
        em.close();
    }

    @Override
    public Partida postPartidaByID(Long id) {
        EntityManager em = emf.createEntityManager();
        Partida partida = em.find(Partida.class, id);
        em.close();
        return partida;
    }

    @Override
    public List<Partida> postAllPartida() {
        EntityManager em = emf.createEntityManager();
        Query query = em.createQuery("SELECT a FROM Partida a");
        List<Partida> partidas = query.getResultList();
        em.close();
        return partidas;
    }

    @Override
    public void putPartida(Partida partida) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.merge(partida);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deletePartida(Partida partida) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        partida = em.merge(partida);
        em.remove(partida);
        em.getTransaction().commit();
        em.close();
    }
}
