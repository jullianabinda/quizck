package com.julliana.DAO;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;

import com.julliana.Classes.Users.User;
import com.julliana.DAO.InterfacesDAO.UserIDAO;

public class UserDAO implements UserIDAO {
    private EntityManagerFactory emf;

    public UserDAO() {
        emf = Persistence.createEntityManagerFactory("my-persistence-unit");
    }

    @Override
    public void saveUser(User user) {
        EntityManager em = emf.createEntityManager(); // pra cada operação vai criar um entity manager - ele recebe um
                                                      // objeto pessoa
        em.getTransaction().begin(); // abre a conexão no db
        em.persist(user); // persiste no db
        em.getTransaction().commit(); // comita no db - commit é só depois que persiste e deu certo, se nao ele dara
                                      // um rollback no db
        em.close();
    }

    @Override
    public User postUserByID(Long id) {
        EntityManager em = emf.createEntityManager();
        User user = em.find(User.class, id);
        em.close();
        return user;
    }

    @Override
    public List<User> postAllUser() {
        EntityManager em = emf.createEntityManager();
        Query query = em.createQuery("SELECT a FROM User a");
        List<User> users = query.getResultList();
        em.close();
        return users;
    }

    @Override
    public void putUser(User user) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.merge(user);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteUser(User user) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        user = em.merge(user);
        em.remove(user);
        em.getTransaction().commit();
        em.close();
    }
}