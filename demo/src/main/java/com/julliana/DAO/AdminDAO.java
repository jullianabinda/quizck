package com.julliana.DAO;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.julliana.Classes.Users.Admin;
import com.julliana.DAO.InterfacesDAO.AdminIDAO;

import java.util.List;

public class AdminDAO implements AdminIDAO {
    private EntityManagerFactory emf;

    public AdminDAO() {
        emf = Persistence.createEntityManagerFactory("my-persistence-unit");
    }

    @Override
    public void saveAdmin(Admin admin) {
        EntityManager em = emf.createEntityManager(); // pra cada operação vai criar um entity manager - ele recebe um
                                                      // objeto pessoa
        em.getTransaction().begin(); // abre a conexão no db
        em.persist(admin); // persiste no db
        em.getTransaction().commit(); // comita no db - commit é só depois que persiste e deu certo, se nao ele dara
                                      // um rollback no db
        em.close();
    }

    @Override
    public Admin postAdminByID(Long id) {
        EntityManager em = emf.createEntityManager();
        Admin admin = em.find(Admin.class, id);
        em.close();
        return admin;
    }

    public Admin postAdminByLogin(String login) {
        EntityManager em = emf.createEntityManager();
        String jpql = "SELECT a FROM Admin a WHERE a.login = :login"; // construi uma consulta JPQL para selecionar o admin que tem o login fornecido pelo parametro do metodo
        // o :login é um marcador de posição para o valor do login na consulta
        TypedQuery<Admin> query = em.createQuery(jpql, Admin.class); // crio um objeto typed query com a consulta JPQL
        query.setParameter("login", login); // defino o valor do parametro login usando o metodo set parameter
        //Admin admin = query.getSingleResult(); // uso o get single result para obter o unico resultado da consulta, assumindo que o login é exclusivo para cada admin
        List<Admin> admins = query.getResultList(); //obtendo uma lista de resultados da consulta
        em.close();

        if (!admins.isEmpty()) { //verifico se a lista não está vazia
            return admins.get(0); //se não estiver vazia, retorno o primeiro admin no index 0 (que é pra ser o unico)
        }
        return null; //se retornar null é pq nao tinha aquele login salvo
    }

    @Override
    public List<Admin> postAllAdmin() {
        EntityManager em = emf.createEntityManager();
        Query query = em.createQuery("SELECT a FROM Admin a");
        List<Admin> admins = query.getResultList();
        em.close();
        return admins;
    }

    @Override
    public void putAdmin(Admin admin) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.merge(admin);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deleteAdmin(Admin admin) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        admin = em.merge(admin);
        em.remove(admin);
        em.getTransaction().commit();
        em.close();
    }
}