package com.julliana.DAO;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.Query;
import javax.persistence.TypedQuery;

import com.julliana.Classes.Users.Player;
import com.julliana.Classes.Users.Player;
import com.julliana.DAO.InterfacesDAO.PlayerIDAO;

public class PlayerDAO implements PlayerIDAO {
    private EntityManagerFactory emf;

    public PlayerDAO() {
        emf = Persistence.createEntityManagerFactory("my-persistence-unit");
    }

    @Override
    public void savePlayer(Player player) {
        EntityManager em = emf.createEntityManager(); // pra cada operação vai criar um entity manager - ele recebe um
                                                      // objeto pessoa
        em.getTransaction().begin(); // abre a conexão no db
        em.persist(player); // persiste no db
        em.getTransaction().commit(); // comita no db - commit é só depois que persiste e deu certo, se nao ele dara
                                      // um rollback no db
        em.close();
    }

    @Override
    public Player postPlayerByID(Long id) {
        EntityManager em = emf.createEntityManager();
        Player player = em.find(Player.class, id);
        em.close();
        return player;
    }

        public Player postPlayerByLogin(String login) {
        EntityManager em = emf.createEntityManager();
        String jpql = "SELECT a FROM Player a WHERE a.login = :login"; // construi uma consulta JPQL para selecionar o player que tem o login fornecido pelo parametro do metodo
        // o :login é um marcador de posição para o valor do login na consulta
        TypedQuery<Player> query = em.createQuery(jpql, Player.class); // crio um objeto typed query com a consulta JPQL
        query.setParameter("login", login); // defino o valor do parametro login usando o metodo set parameter
        //Player player = query.getSingleResult(); // uso o get single result para obter o unico resultado da consulta, assumindo que o login é exclusivo para cada player
        List<Player> players = query.getResultList(); //obtendo uma lista de resultados da consulta
        em.close();

        if (!players.isEmpty()) { //verifico se a lista não está vazia
            return players.get(0); //se não estiver vazia, retorno o primeiro admin no index 0 (que é pra ser o unico)
        }
        return null; //se retornar null é pq nao tinha aquele login salvo
    }

    @Override
    public List<Player> postAllPlayer() {
        EntityManager em = emf.createEntityManager();
        Query query = em.createQuery("SELECT a FROM Player a");
        List<Player> players = query.getResultList();
        em.close();
        return players;
    }

    @Override
    public void putPlayer(Player player) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        em.merge(player);
        em.getTransaction().commit();
        em.close();
    }

    @Override
    public void deletePlayer(Player player) {
        EntityManager em = emf.createEntityManager();
        em.getTransaction().begin();
        player = em.merge(player);
        em.remove(player);
        em.getTransaction().commit();
        em.close();
    }
}