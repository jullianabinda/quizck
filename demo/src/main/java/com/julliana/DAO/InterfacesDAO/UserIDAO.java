package com.julliana.DAO.InterfacesDAO;

import java.util.List;

import com.julliana.Classes.Users.User;

public interface UserIDAO {
    public void saveUser(User user);
    public User postUserByID(Long id);
    public List<User> postAllUser();
    public void putUser(User user);
    public void deleteUser(User user);
}