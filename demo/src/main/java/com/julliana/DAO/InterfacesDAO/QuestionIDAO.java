package com.julliana.DAO.InterfacesDAO;

import java.util.List;

import com.julliana.Classes.Games.Question;

public interface QuestionIDAO {
    public void getQuestion(Question question);
    public Question postQuestionByID(Long id);
    public List<Question> postAllQuestion();
    public void putQuestion(Question question);
    public void deleteQuestion(Question question);
}