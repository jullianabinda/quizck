package com.julliana.DAO.InterfacesDAO;

import java.util.List;

import com.julliana.Classes.Games.Partida;

public interface PartidaIDAO {
    public void getPartida(Partida partida);

    public Partida postPartidaByID(Long id);

    public List<Partida> postAllPartida();

    public void putPartida(Partida partida);

    public void deletePartida(Partida partida);
}
