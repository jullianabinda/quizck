package com.julliana.DAO.InterfacesDAO;

import java.util.List;
import com.julliana.Classes.Users.Admin;

public interface AdminIDAO {
    public void saveAdmin(Admin admin);
    public Admin postAdminByID(Long id);
    public List<Admin> postAllAdmin();
    public void putAdmin(Admin admin);
    public void deleteAdmin(Admin admin);
}