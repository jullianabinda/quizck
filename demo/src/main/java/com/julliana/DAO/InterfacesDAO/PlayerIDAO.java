package com.julliana.DAO.InterfacesDAO;

import java.util.List;

import com.julliana.Classes.Users.Player;

public interface PlayerIDAO {
    public void savePlayer(Player player);
    public Player postPlayerByID(Long id);
    public List<Player> postAllPlayer();
    public void putPlayer(Player player);
    public void deletePlayer(Player player);
}