package com.julliana.DAO.InterfacesDAO;

import java.util.List;

import com.julliana.Classes.Games.Ranking;

public interface RankingIDAO {
    public void saveRanking(Ranking ranking);

    public Ranking postRankingByID(Long id);

    public List<Ranking> postAllRanking();

    public void putRanking(Ranking ranking);

    public void deleteRanking(Ranking ranking);
}