package com.julliana.Classes.Users;

import java.util.ArrayList;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.mindrot.jbcrypt.BCrypt;
import com.julliana.DAO.AdminDAO;
import com.julliana.DAO.UserDAO;

@Entity
public class Admin extends User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    public Admin(String name, String login, String password, Boolean logado) {
        super(name, login, password, logado);
    }

    public Admin() {
    }

    @Override
    public String toString() {
        return "\nAdmin [ID: " + id + "| Name: " + name + " | Login: " + login + " | Password: " + password + "]";
    }

    @Override
    public void newUser() {
        AdminDAO aDAO = new AdminDAO();
        ArrayList<Admin> admins = (ArrayList<Admin>) aDAO.postAllAdmin();
        System.out.println("\n----- NEW ADMIN -----");
        System.out.println("Name: ");
        this.name = leitor.nextLine();
        boolean loginExiste = true;
        while (loginExiste) {
            System.out.println("Login: ");
            this.login = leitor.nextLine();
            Admin loginTemp = aDAO.postAdminByLogin(login);
            if (loginTemp == null) {
                loginExiste = false;
            } else {
                System.out.println("Login já existe na base de dados!");
            }
        }
        System.out.println("Password: ");
        this.password = leitor.nextLine();
        String passwordCrypt = BCrypt.hashpw(this.password, BCrypt.gensalt());
        System.out.println("\n----- NEW ADMIN SUCCESSFULLY REGISTERED -----");
        this.logado = false;
        Admin moderator = new Admin(this.name, this.login, passwordCrypt, this.logado);
        aDAO.saveAdmin(moderator);
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Boolean loginAdmin(String login) {
        AdminDAO aDAO = new AdminDAO();
        ArrayList<Admin> admins = (ArrayList<Admin>) aDAO.postAllAdmin();
        for (Admin a : admins) {
            if (login.equals(a.getLogin())) {
                System.out.println("Password: ");
                String passwordInf = leitor.nextLine();
                String passwordSaved = a.getPassword();
                if (BCrypt.checkpw(passwordInf, passwordSaved)) {
                    a.setLogado(true);
                    aDAO.putAdmin(a);
                    return true;
                }
            } else {
                System.out.println("----- LOGIN NÃO ENCONTRADO!");
            }
        }
        return false;
    }
}