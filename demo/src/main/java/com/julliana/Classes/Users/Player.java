package com.julliana.Classes.Users;

import java.util.ArrayList;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.mindrot.jbcrypt.BCrypt;

import com.julliana.DAO.UserDAO;
import com.julliana.DAO.AdminDAO;
import com.julliana.DAO.PlayerDAO;

@Entity
public class Player extends User {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    private int level;


    public Player(String name, String login, String password, Boolean logado, int level) {
        super(name, login, password, logado);
        this.level = level;
    }

    public Player(int level) {
        this.level = level;
    }

    public Player() {
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    @Override
    public String toString() {
        return "\nPlayer [ID: " + id + " | Name: " + name + " | Login: " + login + " | Password: " + password
                + " | Level: " + level + "]";
    }

    @Override
    public void newUser() {
        PlayerDAO pDAO = new PlayerDAO();
        ArrayList<Player> gamers = (ArrayList<Player>) pDAO.postAllPlayer();
        System.out.println("\n----- NEW PLAYER -----");
        System.out.println("Name: ");
        this.name = leitor.nextLine();
        boolean loginExiste = true;
        while (loginExiste) {
            System.out.println("Login: ");
            this.login = leitor.nextLine();
                Player loginTemp = pDAO.postPlayerByLogin(login);
                if (loginTemp == null) {
                    loginExiste = false;
                } else {
                    System.out.println("Login já existe na base de dados!");
            }
        }
        System.out.println("Password: ");
        this.password = leitor.nextLine();
        String passwordCrypt = BCrypt.hashpw(this.password, BCrypt.gensalt());
        System.out.println("\n----- NEW ADMIN SUCCESSFULLY REGISTERED -----");
        this.logado = false;
        this.level = 0;
        Player gamer = new Player(this.name, this.login, passwordCrypt, this.logado, level);
        pDAO.savePlayer(gamer);
    }

    public Boolean loginPlayer(String login) {
        PlayerDAO pDAO = new PlayerDAO();
        ArrayList<Player> gamers = (ArrayList<Player>) pDAO.postAllPlayer();
        for (Player p : gamers) {
            if (login.equals(p.getLogin())) {
                System.out.println("Password: ");
                String passwordInf = leitor.nextLine();
                String passwordSaved = p.getPassword();
                if (BCrypt.checkpw(passwordInf, passwordSaved)) {
                    p.setLogado(true);
                    pDAO.putPlayer(p);
                    return true;
                }
            } else {
                System.out.println("----- LOGIN NÃO ENCONTRADO!");
            }
        }
        return false;
    }
}