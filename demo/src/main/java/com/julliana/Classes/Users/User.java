package com.julliana.Classes.Users;

import java.util.ArrayList;
import java.util.Scanner;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import org.mindrot.jbcrypt.BCrypt;

import com.julliana.DAO.AdminDAO;
import com.julliana.DAO.UserDAO;

@Entity
public class User {
    @Id@GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    static Scanner leitor = new Scanner(System.in);
    protected String name, login, password;
    protected Boolean logado;

    public User(String name, String login, String password, Boolean logado) {
        this.name = name;
        this.login = login;
        this.password = password;
        this.logado = logado;
    }

    public User() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public String toString() {
        return "\nUser [ID: " + id + "name=" + name + ", login=" + login + ", password=" + password + "]";
    }

    public void newUser() {
        System.out.println("\n----- NEW USER -----");
        System.out.println("Name: ");
        this.name = leitor.nextLine();
        System.out.println("Login: ");
        this.login = leitor.nextLine();
        System.out.println("Password: ");
        this.password = leitor.nextLine();
        String passwordCrypt = BCrypt.hashpw(this.password, BCrypt.gensalt());
        this.logado = false;

        User user = new User(name, login, passwordCrypt, logado);
    }

        public Boolean loginUser(String login) {
        UserDAO uDAO = new UserDAO();
        ArrayList<User> users = (ArrayList<User>) uDAO.postAllUser();
        for (User u : users) {
            if (login.equals(u.getLogin())) {
                System.out.println("Password: ");
                String passwordInf = leitor.nextLine();
                String passwordSaved = u.getPassword();
                if (BCrypt.checkpw(passwordInf, passwordSaved)) {
                    u.setLogado(true);
                    return true;
                }
            }
        }
        return false;
    }

        public Boolean getLogado() {
            return logado;
        }

        public void setLogado(Boolean logado) {
            this.logado = logado;
        }   
}