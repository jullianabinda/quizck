package com.julliana.Classes.Games;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Scanner;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import com.julliana.Classes.Interfaces.Play;
import com.julliana.DAO.PartidaDAO;
import com.julliana.DAO.QuestionDAO;

@Entity
public class Partida implements Play {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    protected LocalDate dataInicio;
    protected String login;
    protected String question;
    protected int chosenAnswer, score;

    public Partida(LocalDate dataInicio, String login, String question, int chosenAnswer, int score) {
        this.dataInicio = dataInicio;
        this.login = login;
        this.question = question;
        this.chosenAnswer = chosenAnswer;
        this.score = score;
    }

    public Partida() {
    }

    public LocalDate getDataInicio() {
        return dataInicio;
    }

    public void setDataInicio(LocalDate dataInicio) {
        this.dataInicio = dataInicio;
    }

    /*
     * public Player getGamers() {
     * return gamers;
     * }
     * 
     * public void setGamers(Player gamers) {
     * this.gamers = gamers;
     * }
     */

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public int getChosenAnswer() {
        return chosenAnswer;
    }

    public void setChosenAnswer(int chosenAnswer) {
        this.chosenAnswer = chosenAnswer;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    @Override
    public String toString() {
        return "Partida [ID: " + id + " | dataInicio=" + dataInicio + ", question=" + question
                + ", chosenAnswer="
                + chosenAnswer + ", score=" + score + "]";
    }

    public void startGame(String login) {
        Scanner leitor = new Scanner(System.in);
        QuestionDAO qDAO = new QuestionDAO();
        ArrayList<Question> queries = (ArrayList<Question>) qDAO.postAllQuestion();

        for (Question q : queries) {
            this.dataInicio = LocalDate.now();
            this.login = login;
            this.question = q.getQuery();
            System.out.println(q.getQuery());
            System.out.println("1 - " + q.getFirstAnswer());
            System.out.println("2 - " + q.getSecondAnswer());
            System.out.println("3 - " + q.getThirdAnswer());
            System.out.println("\nAnswer: ");
            int chosenAnswer = Integer.parseInt(leitor.nextLine());
            if (q.validateAnswer(chosenAnswer)) {
                System.out.println("Acertou!");
                this.score = q.getValue();
            } else {
                System.out.println("Errou!");
                this.score = 0;
            }
            Partida partida = new Partida(dataInicio, login, question, chosenAnswer, score);
            PartidaDAO pDAO = new PartidaDAO();
            pDAO.getPartida(partida);
        }
    }
}