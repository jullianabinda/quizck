package com.julliana.Classes.Games;

import java.time.LocalDate;

public class VetorMultiTipo implements Comparable<VetorMultiTipo>{
    private String user;
    private int score;
    private LocalDate data;
    
    public VetorMultiTipo(int score, String user, LocalDate data) {
        this.user = user;
        this.score = score;
        this.data = data;
    }

    public VetorMultiTipo(int score, String user) {
        this.user = user;
        this.score = score;
    }

    

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public LocalDate getData() {
        return data;
    }

    public void setData(LocalDate data) {
        this.data = data;
    }

    @Override
    public int compareTo(VetorMultiTipo o) {
        return Integer.compare(this.score, o.score);
    }

    @Override
    public String toString() {
        return "\nVetorMultiTipo [user=" + user + ", score=" + score + ", data=" + data + "]";
    }       
}
