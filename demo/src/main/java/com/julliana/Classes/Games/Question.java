package com.julliana.Classes.Games;

import java.util.Scanner;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import com.julliana.DAO.QuestionDAO;

@Entity
public class Question {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;
    static Scanner leitor = new Scanner(System.in);
    protected String query, firstAnswer, secondAnswer, thirdAnswer;
    protected int value, correct, difficultyLevel;

    public Question(String query, String firstAnswer, String secondAnswer, String thirdAnswer,
            int difficultyLevel, int value, int correct) {
        this.query = query;
        this.firstAnswer = firstAnswer;
        this.secondAnswer = secondAnswer;
        this.thirdAnswer = thirdAnswer;
        this.difficultyLevel = difficultyLevel;
        this.value = value;
        this.correct = correct;
    }

    public Question() {
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getFirstAnswer() {
        return firstAnswer;
    }

    public void setFirstAnswer(String firstAnswer) {
        this.firstAnswer = firstAnswer;
    }

    public String getSecondAnswer() {
        return secondAnswer;
    }

    public void setSecondAnswerAnswer1(String secondAnswer) {
        this.secondAnswer = secondAnswer;
    }

    public String getThirdAnswer() {
        return thirdAnswer;
    }

    public void setThirdAnswerAnswer2(String thirdAnswer) {
        this.thirdAnswer = thirdAnswer;
    }

    public int getDifficultyLevel() {
        return difficultyLevel;
    }

    public void setDifficultyLevel(int difficultyLevel) {
        this.difficultyLevel = difficultyLevel;
    }

    public int getValue() {
        return value;
    }

    public void setValue(int value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return "Question [ID: "+ id+ " | Query: " + query + " | Right Answer: " + firstAnswer + " | Wrong Answer 1: " + secondAnswer
                + " | Wrong Answer 2: " + thirdAnswer + " | Difficulty Level: " + difficultyLevel
                + " | Value: " + value + "]";
    }

    public void createQuestion() {
        System.out.println("\n----- CREATE NEW QUESTION -----\n");
        System.out.println("Query: ");
        this.query = leitor.nextLine();
        System.out.println("First answer: ");
        this.firstAnswer = leitor.nextLine();
        System.out.println("Second answer: ");
        this.secondAnswer = leitor.nextLine();
        System.out.println("Third answer: ");
        this.thirdAnswer = leitor.nextLine();
        while (difficultyLevel <= 0 || difficultyLevel > 3) {
            System.out.println("Difficulty level: \n1 - EASY\n2 - MEDIUM\n3 - HARD\n");
            this.difficultyLevel = Integer.parseInt(leitor.nextLine());
        }
        System.out.println("Value: ");
        this.value = Integer.parseInt(leitor.nextLine());
        System.out.println("Right answer: ");
        this.correct = Integer.parseInt(leitor.nextLine());

        Question question = new Question(query, firstAnswer, secondAnswer, thirdAnswer, difficultyLevel, value,
                correct);
        QuestionDAO qDAO = new QuestionDAO();
        qDAO.getQuestion(question);
    }

    public int getRight() {
        return correct;
    }

    public void setRight(int correct) {
        this.correct = correct;
    }

    public Boolean validateAnswer(int chosenAnswer) {
        if (chosenAnswer == this.correct) {
            return true;
        } else {
            return false;
        }
    }
}