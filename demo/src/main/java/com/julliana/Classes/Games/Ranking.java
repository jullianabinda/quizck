package com.julliana.Classes.Games;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

import com.julliana.Classes.Users.Player;
import com.julliana.DAO.PartidaDAO;

public class Ranking {
    protected Player gamer;
    protected int points;

    public Ranking(Player gamer, int points) {
        this.gamer = gamer;
        this.points = points;
    }

    public Ranking() {
    }

    public Player getGamer() {
        return gamer;
    }

    public void setGamer(Player gamer) {
        this.gamer = gamer;
    }

    public int getPoints() {
        return points;
    }

    public void setPoints(int points) {
        this.points = points;
    }

    @Override
    public String toString() {
        return "Ranking [gamer=" + gamer + ", points=" + points + "]";
    }

    public void rankingUserByDay() {
        PartidaDAO pDAO = new PartidaDAO();
        ArrayList<Partida> partidas = (ArrayList<Partida>) pDAO.postAllPartida();
        ArrayList<VetorMultiTipo> rankingGeneral = new ArrayList<>();
        String userTemp = "";
        int score = 0;

        for (Partida p : partidas) {
            String user = p.login;
            LocalDate data = p.dataInicio;

            if (user.equals(userTemp)) {
                score += p.score;
                System.out.println("entrou no if");
            } else {
                if (!userTemp.isEmpty()) {
                    boolean userExists = false;
                    for (VetorMultiTipo vetor : rankingGeneral) {
                        if (vetor.getUser().equals(userTemp)) {
                            userExists = true;
                            System.out.println(score + " 1 sout " + vetor.getUser());
                            int updatedScore = vetor.getScore() + score;
                            vetor.setScore(updatedScore);
                            System.out.println(updatedScore + " 2 sout");
                            break;
                        }
                    }
                    if (!userExists) {
                        VetorMultiTipo posicao = new VetorMultiTipo(score, userTemp, data);
                        rankingGeneral.add(posicao);
                    }
                }

                score = p.score;
                System.out.println("entrou no else");
                userTemp = user;
            }
        }

        if (!userTemp.isEmpty()) {
            boolean userExists = false;
            for (VetorMultiTipo vetor : rankingGeneral) {
                if (vetor.getUser().equals(userTemp)) {
                    userExists = true;
                    System.out.println(score + " 1 sout " + vetor.getUser());
                    int updatedScore = vetor.getScore() + score;
                    vetor.setScore(updatedScore);
                    System.out.println(updatedScore + " 2 sout");
                    break;
                }
            }
            if (!userExists) {
                VetorMultiTipo posicao = new VetorMultiTipo(score, userTemp);
                rankingGeneral.add(posicao);
            }
        }

        Collections.sort(rankingGeneral, Collections.reverseOrder());
        System.out.println(rankingGeneral);
    }
}