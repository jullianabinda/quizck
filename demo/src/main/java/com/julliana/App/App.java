package com.julliana.App;

import java.util.ArrayList;
import java.util.Scanner;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

import org.mindrot.jbcrypt.BCrypt;

import com.julliana.Classes.Games.Partida;
import com.julliana.Classes.Games.Question;
import com.julliana.Classes.Games.Ranking;
import com.julliana.Classes.Users.Admin;
import com.julliana.Classes.Users.Player;
import com.julliana.Classes.Users.User;
import com.julliana.DAO.AdminDAO;
import com.julliana.DAO.PlayerDAO;
import com.julliana.DAO.QuestionDAO;
import com.julliana.DAO.UserDAO;

public class App {
    static Scanner leitor = new Scanner(System.in);

    public static void main(String[] args) {
        EntityManagerFactory emf = Persistence.createEntityManagerFactory("my-persistence-unit");
        EntityManager em = emf.createEntityManager();
        UserDAO uDAO = new UserDAO();
        ArrayList<User> users = (ArrayList<User>) uDAO.postAllUser();

        try {
            int menu;
            System.out.println("----- WELCOME TO QUIZCK -----");
            do {
                System.out.println(
                        "\n1 - CRIAR JOGADOR\n2 - CRIAR ADMIN\n3 - FAZER LOGIN\n4 - FAZER LOGOFF\n5 - ALTERAR SENHA\n6 - ALTERAR NOME\n7 - LISTAR USUÁRIOS\n8 - DELETAR USUÁRIO\n9 - RANKING\n10 - CRIAR PERGUNTA\n11 - ATUALIZAR PERGUNTA\n12 - LISTAR PERGUNTAS\n13 - DELETAR PERGUNTA\n14 - JOGAR\n15 - SAIR");

                System.out.println("\n----- SELECIONE A OPÇÃO QUE DESEJA: ");
                menu = Integer.parseInt(leitor.nextLine());
                User u = new User();

                switch (menu) {
                    case 1: // criar jogador
                        criarUsuario(1);
                        break;
                    case 2: // criar admin
                        criarUsuario(2);
                        break;
                    case 3: // fazer login
                        loginUsuario(1);
                        break;
                    case 4: // fazer logoff
                        loginUsuario(2);
                        break;
                    case 5: // alterar senha
                        updatePassword();
                        break;
                    case 6: // alterar nome
                        updateName();
                        break;
                    case 7: // listar usuarios
                        listarUsuarios();
                        break;
                    case 8: // deletar usuario
                        deleteUser();
                        break;
                    case 9: // ranking
                        Ranking ranking = new Ranking();
                        ranking.rankingUserByDay();
                        break;
                    case 10: // criar pergunta
                        criarPergunta();
                        break;
                    case 11: // atualizar pergunta
                        alterarPergunta();
                        break;
                    case 12: // listar perguntas
                        listarPerguntas();
                        break;
                    case 13: // deletar pergunta
                        deletePergunta();
                        break;
                    case 14: // jogar
                        iniciarPartida();
                        break;
                    case 15: // sair
                        System.out.println("----- CRUJ CRUJ TCHAU...");
                        break;
                    default:
                        System.out.println("----- OPÇÃO INVÁLIDA!");
                        break;
                }
            } while (menu != 15);
        } catch (Exception e) {
            System.out.println("-------- ERRO FOI NO MAIN --------: " + e);
        }
    }

    public static void criarUsuario(int u) {
        try {
            if (u == 1) {
                Player gamer = new Player();
                gamer.newUser();
                return;
            } else if (u == 2) {
                Admin admin = new Admin();
                admin.newUser();
                return;
            } else {
                System.out.println("----- OPÇÃO INVÁLIDA!");
                return;
            }
        } catch (Exception e) {
            System.out.println("-------- ERRO FOI NO CRIAR USUÁRIO --------: " + e);
        }
    }

    public static void loginUsuario(int inOff) {
        try {
            System.out.println("Login: ");
            String loginInf = leitor.nextLine();
            UserDAO uDAO = new UserDAO();
            ArrayList<User> users = (ArrayList<User>) uDAO.postAllUser();
            for (User u : users) {
                if (u.loginUser(loginInf)) {
                    if (inOff == 1) {
                        u.setLogado(true);
                        uDAO.putUser(u);
                        System.out.println("----- USUÁRIO LOGADO!");
                        return;
                    } else if (inOff == 2) {
                        u.setLogado(false);
                        uDAO.putUser(u);
                        System.out.println("----- USUÁRIO DESLOGADO!");
                        return;
                    } else {
                        System.out.println("----- OPÇÃO INVÁLIDA!");
                        return;
                    }
                }
            }
            System.out.println("----- NÃO FOI POSSÍVEL FAZER LOGIN!");
        } catch (Exception e) {
            System.out.println("-------- ERRO FOI NO LOGAR USUARIO --------: " + e);
        }
    }

    public static void updatePassword() {
        try {
            System.out.println("Login: ");
            String loginInf = leitor.nextLine();
            UserDAO uDAO = new UserDAO();
            ArrayList<User> users = (ArrayList<User>) uDAO.postAllUser();
            for (User u : users) {
                if (loginInf.equals(u.getLogin())) {
                    if (u.getLogado()) {
                        System.out.println("Nova senha: ");
                        String newPassword = leitor.nextLine();
                        System.out.println("Confirme a nova senha: ");
                        String confirmPassword = leitor.nextLine();
                        if (newPassword.equals(confirmPassword)) {
                            String passwordCrypt = BCrypt.hashpw(newPassword, BCrypt.gensalt());
                            u.setPassword(passwordCrypt);
                            uDAO.putUser(u);
                            System.out.println("Senha alterada com sucesso!");
                        } else {
                            System.out.println("----- SENHAS NÃO CONFEREM!");
                        }
                    } else {
                        System.out.println("----- USUÁRIO NÃO ESTÁ LOGADO!");
                    }
                    return;
                }
            }
            System.out.println("----- LOGIN NÃO ENCONTRADO!");
        } catch (Exception e) {
            System.out.println("-------- ERRO FOI NO UPDATE PASSWORD --------: " + e);
        }
    }

    public static void updateName() {
        try {
            System.out.println("Login: ");
            String loginInf = leitor.nextLine();
            UserDAO uDAO = new UserDAO();
            ArrayList<User> users = (ArrayList<User>) uDAO.postAllUser();
            for (User u : users) {
                if (loginInf.equals(u.getLogin()) && u.getLogado()) {
                    System.out.println("Novo nome: ");
                    String newName = leitor.nextLine();
                    System.out.println("Confirme o novo nome: ");
                    String confirmName = leitor.nextLine();
                    if (newName.equals(confirmName)) {
                        u.setName(confirmName);
                        uDAO.putUser(u);
                        System.out.println("Nome alterado com sucesso!");
                        return;
                    } else {
                        System.out.println("----- NOMES NÃO CONFEREM!");
                    }
                    return;
                }
            }
            System.out.println("-----LOGIN NÃO ENCONTRADO OU NÃO ESTÁ LOGADO!");
        } catch (Exception e) {
            System.out.println("-------- ERRO FOI NO UPDATE NAME --------: " + e);
        }
    }

    public static void listarUsuarios() {
        try {
            System.out.println("Login: ");
            String loginInf = leitor.nextLine();
            AdminDAO aDAO = new AdminDAO();
            UserDAO uDAO = new UserDAO();
            PlayerDAO pDAO = new PlayerDAO();
            ArrayList<Admin> admins = (ArrayList<Admin>) aDAO.postAllAdmin();
            for (Admin a : admins) {
                if (loginInf.equals(a.getLogin()) && a.getLogado()) {
                    System.out.println("1 - LIST PLAYERS\n2 - LIST ADMINs\n3 - LIST USERS");
                    int op = Integer.parseInt(leitor.nextLine());
                    if (op == 1) {
                        ArrayList<Player> gamers = (ArrayList<Player>) pDAO.postAllPlayer();
                        System.out.println(gamers);
                    } else if (op == 2) {
                        ArrayList<Admin> moderators = (ArrayList<Admin>) aDAO.postAllAdmin();
                        System.out.println(moderators);
                    } else if (op == 3) {
                        ArrayList<User> users = (ArrayList<User>) uDAO.postAllUser();
                        System.out.println(users);
                    } else {
                        System.out.println("Invalid option!");
                    }
                    return;
                }
            }
        } catch (Exception e) {
            System.out.println("-------- ERRO FOI NO LISTAR USUÁRIOS --------: " + e);
        }
    }

    public static void deleteUser() {
        try {
            UserDAO uDAO = new UserDAO();
            ArrayList<User> users = (ArrayList<User>) uDAO.postAllUser();
            for (User u : users) {
                System.out.println(u);
            }
            System.out.println("ID do usuário que deseja deletar: ");
            Long idInf = Long.parseLong(leitor.nextLine());
            System.out.println("Login: ");
            String loginInf = leitor.nextLine();
            for (User u : users) {
                if (loginInf.equals(u.getLogin()) && u.getLogado()) {
                    User userDeleted = uDAO.postUserByID(idInf);
                    uDAO.deleteUser(userDeleted);
                    System.out.println("----- USUÁRIO DELETADO!!");
                    return;
                }
            }
        } catch (Exception e) {
            System.out.println("-------- ERRO FOI NO DELETE USERS --------: " + e);
        }
    }

    public static void iniciarPartida() {
        try {
            Partida partida = new Partida();
            System.out.println("Login: ");
            String loginInf = leitor.nextLine();
            UserDAO uDAO = new UserDAO();
            ArrayList<User> users = (ArrayList<User>) uDAO.postAllUser();
            for (User u : users) {
                if (loginInf.equals(u.getLogin()) && u.getLogado()) {
                    partida.startGame(loginInf);
                    return;
                }
            }
            System.out.println("-----LOGIN NÃO ENCONTRADO OU NÃO ESTÁ LOGADO!");
        } catch (Exception e) {
            System.out.println("-------- ERRO FOI NO INICIAR PARTIDA --------: " + e);
        }
    }

    public static void criarPergunta() {
        try {
            System.out.println("Login: ");
            String loginInf = leitor.nextLine();
            AdminDAO aDAO = new AdminDAO();
            ArrayList<Admin> admins = (ArrayList<Admin>) aDAO.postAllAdmin();
            for (Admin a : admins) {
                if (loginInf.equals(a.getLogin()) && a.getLogado()) {
                    Question question = new Question();
                    question.createQuestion();
                    System.out.println("----- PERGUNTA CRIADA!");
                    return;
                }
            }
            System.out.println("-----LOGIN NÃO ENCONTRADO OU NÃO ESTÁ LOGADO!");
        } catch (Exception e) {
            System.out.println("-------- ERRO FOI NO CREATE QUESTION --------: " + e);
        }
    }

    public static void alterarPergunta() {
        try {
            System.out.println("Login: ");
            String loginInf = leitor.nextLine();
            AdminDAO aDAO = new AdminDAO();
            ArrayList<Admin> admins = (ArrayList<Admin>) aDAO.postAllAdmin();
            for (Admin a : admins) {
                if (loginInf.equals(a.getLogin()) && a.getLogado()) {
                    QuestionDAO qDAO = new QuestionDAO();
                    ArrayList<Question> queries = (ArrayList<Question>) qDAO.postAllQuestion();
                    for (Question q : queries) {
                        System.out.println(q);
                    }
                    System.out.println("Qual ID da pergunta que quer alterar? ");
                    Long idInf = Long.parseLong(leitor.nextLine());
                    Question querySelected = qDAO.postQuestionByID(idInf);
                    System.out.println(
                            "\n1 - ALTERAR A PERGUNTA\n2 - ALTERAR A PRIMEIRA RESPOSTA\n3 - ALTERAR A SEGUNDA RESPOSTA\n4 - ALTERAR A TERCEIRA RESPOSTA\n5 - ALTERAR A DIFICULDADE\n6 - ALTERAR O VALOR\n");
                    int op = Integer.parseInt(leitor.nextLine());
                    if (op == 1) {
                        System.out.println("Nova pergunta: ");
                        String novaPerg = leitor.nextLine();
                        querySelected.setQuery(novaPerg);
                        qDAO.putQuestion(querySelected);
                        System.out.println("Pergunta alterada com sucesso!");
                        return;
                    } else if (op == 2) {
                        System.out.println("Nova primeira resposta: ");
                        String novaFirst = leitor.nextLine();
                        querySelected.setFirstAnswer(novaFirst);
                        qDAO.putQuestion(querySelected);
                        System.out.println("Primeira resposta alterada com sucesso!");
                        return;
                    } else if (op == 3) {
                        System.out.println("Nova segunda resposta: ");
                        String novaSecond = leitor.nextLine();
                        querySelected.setSecondAnswerAnswer1(novaSecond);
                        qDAO.putQuestion(querySelected);
                        System.out.println("Segunda resposta alterada com sucesso!");
                        return;
                    } else if (op == 4) {
                        System.out.println("Nova terceira resposta: ");
                        String novaThird = leitor.nextLine();
                        querySelected.setThirdAnswerAnswer2(novaThird);
                        qDAO.putQuestion(querySelected);
                        System.out.println("Terceira resposta alterada com sucesso!");
                        return;
                    } else if (op == 5) {
                        System.out.println("Nova dificuldade (1 - EASY / 2 - MEDIUM / 3 - HARD): ");
                        int novaDificuldade = Integer.parseInt(leitor.nextLine());
                        querySelected.setDifficultyLevel(novaDificuldade);
                        qDAO.putQuestion(querySelected);
                        System.out.println("Dificuldade alterada com sucesso!");
                        return;
                    } else if (op == 6) {
                        System.out.println("Novo valor: ");
                        int novoValor = Integer.parseInt(leitor.nextLine());
                        querySelected.setValue(novoValor);
                        qDAO.putQuestion(querySelected);
                        System.out.println("Valor alterado com sucesso!");
                        return;
                    } else {
                        System.out.println("Opção invalida!");
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("-------- ERRO FOI NO ALTERAR QUESTION --------: " + e);
        }
    }

    public static void listarPerguntas() {
        try {
            System.out.println("Login: ");
            String loginInf = leitor.nextLine();
            AdminDAO aDAO = new AdminDAO();
            QuestionDAO qDAO = new QuestionDAO();
            ArrayList<Admin> admins = (ArrayList<Admin>) aDAO.postAllAdmin();
            for (Admin a : admins) {
                if (loginInf.equals(a.getLogin()) && a.getLogado()) {
                    ArrayList<Question> queries = (ArrayList<Question>) qDAO.postAllQuestion();
                    for (Question q : queries) {
                        System.out.println(q);
                    }
                }
            }
        } catch (Exception e) {
            System.out.println("-------- ERRO FOI NO LISTAR PERGUNTAS --------: " + e);
        }
    }

    public static void deletePergunta() {
        try {
            System.out.println("Login: ");
            String loginInf = leitor.nextLine();
            AdminDAO aDAO = new AdminDAO();
            QuestionDAO qDAO = new QuestionDAO();
            ArrayList<Admin> admins = (ArrayList<Admin>) aDAO.postAllAdmin();
            for (Admin a : admins) {
                if (loginInf.equals(a.getLogin()) && a.getLogado()) {
                    ArrayList<Question> queries = (ArrayList<Question>) qDAO.postAllQuestion();
                    for (Question q : queries) {
                        System.out.println(q);
                    }
                    System.out.println("ID da pergunta que deseja deletar: ");
                    Long idInf = Long.parseLong(leitor.nextLine());
                    Question questionDeleted = qDAO.postQuestionByID(idInf);
                    qDAO.deleteQuestion(questionDeleted);
                    System.out.println("----- PERGUNTA DELETADA!!");
                    return;
                }
            }
        } catch (Exception e) {
            System.out.println("-------- ERRO FOI NO DELETE USERS --------: " + e);
        }
    }
}